# emRiboSeq_context_analysis

Genome wide analysis of emRiboSeq embedded ribonucleotide context preferences.

**Installation**

This analysis code assumes a pre-existing install of Conda and ideally Mamba to bootstrap the working environment. Clone the project into a new location and move into the top-level directory of the project.



Build the environment (only needs to be done on first use)
~~~
$ source ./germinate.sh
~~~

Intialise the envirnoment (done from within the project top-level directory)
~~~
$ source ./init.sh
~~~

Move to the analysis directory
~~~
$ cd analysis/ribo
~~~

Run the default analysis pipeline
~~~
$ snakemake --cores 2
~~~

**Data**

The snakemake rules will automatically acquire the required data. Processing of emRiboSeq data from GEO accessioned FASTQ is possible following the analysis described in Ding et al 2015 DOI: 10.1038/nprot.2015.099. For polymerase wild-type rnh201 null yeast pre-processed BED files are provided for replication of analyses reported in Reijns et al, 2022. The stable DOI for these files is https://doi.org/10.7488/ds/3252.
