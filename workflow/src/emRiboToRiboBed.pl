use strict;
##
# Parameters to tweak for differnt emribo,hydenseq,riboseq methods
my $offset = -1;
my $strandflip = 1;
#
while (<STDIN>){
    chomp;
    my @F = split /\s+/;
    if($F[5]=~/\+/){
	     $F[1] += $offset;
    }
    else{
    	# The minus-one because we are converting a bed end to a bed start.
    	$F[1]=($F[2] - $offset) - 1;
    }
    if($strandflip==1){
      $F[5]=~tr/+-/-+/;
    }
    if($F[1] <= 0){
      next;
    }
    $F[2] = $F[1] + 1;
    print join qq{\t}, @F;
    print "\n";
}
